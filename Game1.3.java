public class Game {
	private final int NoMove = -1;
	public StringBuffer board;

	public Game(String s) {
		board = new StringBuffer(s);
	}

	public Game(StringBuffer s, int position, char player) {
		board = new StringBuffer();
		board.append(s);
		board.setCharAt(position, player);
	}

	public int move(char player) {
		for (int i = 0; i < 9; i++) {
			if (isEmpty(i)) {
				Game game = play(i, player);
				if (game.winner() == player) {
					return i;
				}
			}
		}

		for (int i = 0; i < 9; i++) { // Duplicate Code
			if (isEmpty(i)) {
				return i;
			}
		}
		return NoMove; // Comments (Deodorant)
	}

	public Game play(int move, char player) { // Comments (Deodorant)
		return new Game(this.board, move, player);
	}

	public char winner() {

		for (int pos = 0; pos < 6; pos += 3) {
			if (!isEmpty(pos) && board.charAt(pos) == board.charAt(pos + 1)
					&& board.charAt(pos + 1) == board.charAt(pos + 2)) {
				return board.charAt(pos);
			}
		}
		return '-';
	}

	private boolean isEmpty(int move) {

		if (board.charAt(move) == '-') {
			return true;
		}
		return false;

	}

}
